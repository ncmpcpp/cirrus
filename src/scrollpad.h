/***************************************************************************
 *   Copyright (C) 2008 by Andrzej Rybczak   *
 *   electricityispower@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef HAVE_SCROLLPAD_H
#define HAVE_SCROLLPAD_H

#include "window.h"
#include "strbuffer.h"

class Scrollpad: public Window
{
	public:
		Scrollpad(size_t, size_t, size_t, size_t, const std::string &, Color, Border);
		Scrollpad(const Scrollpad &);
		virtual ~Scrollpad() { }
		
		void Flush();
		std::basic_string<my_char_t> Content() { return itsBuffer.Str(); }
		
		virtual void Refresh();
		virtual void Scroll(Where);
		
		virtual void MoveTo(size_t, size_t);
		virtual void Resize(size_t, size_t);
		virtual void Clear(bool = 1);
		
		template <class T> Scrollpad &operator<<(const T &t)
		{
			itsBuffer << t;
			return *this;
		}
		
		Scrollpad &operator<<(std::ostream &(*os)(std::ostream &));
		
#		ifdef _UTF8
		Scrollpad &operator<<(const char *s);
		Scrollpad &operator<<(const std::string &s);
#		endif // _UTF8
		
		virtual Scrollpad *Clone() const { return new Scrollpad(*this); }
		virtual Scrollpad *EmptyClone() const;
		
	protected:
		virtual void Recreate();
		
		basic_buffer<my_char_t> itsBuffer;
		
		int itsBeginning;
		
		size_t itsRealHeight;
		size_t itsXPos;
};

#endif

